**About Us:**


![image.png](./image.png)

Artiom Bell originally started at UCF as an Industrial Engineering major but shortly transferred to study Electrical Engineering. Artiom is currently employed as a sustaining Engineering intern at Walt Disney World. Artiom also works as a technical writer for a major computer hardware review website. He will finish his bachelor's degree in Electrical Engineering in the Spring of 2010 and hopes to continue on to get a Masters in Electrical Engineering.

abell@ahb.global


![image_1.png](./image_1.png)

Robert Jason Dumbaugh has only worked with electronics and computers for three short years, but it has landed him a role as the Public Relations Manager for a widely known computer hardware review website, a video game tester for AMD in Silicon Valley, California, and been featured in magazines and on websites worldwide for his artwork with respect to computer “case mods.”  In these short three years, Jason has built over 50 computers.  Jason will be receiving a Bachelors’ of Science in Electrical Engineering in Fall 2009 and hopes to continue on to complete a one year Master’s of Science in Electrical Engineering in the Spring of 2011, possibly landing a job in California where he wants to reside.

jdumbaugh@verizon.net


![image_2.png](./image_2.png)

Koltan Riley, II will be working as a Systems Integration/Test Engineering Associate with Lockheed Martin-Missiles & Fire Control in Orlando, Florida upon graduation from the University of Central Florida with a Bachelors’ of Science in Electrical Engineering. Koltan gravitated towards the Engineering Field because of his aptitude in mathematics and science. During his 4 years at UCF, he has participated in several student organizations such as NSBE and the Gospel Choir at UCF and became a member of Omega Psi Phi Fraternity, Inc. were he served as Basileus (Chapter President) & Keeper of Finance (Treasurer).

koltan.riley@gmail.com
